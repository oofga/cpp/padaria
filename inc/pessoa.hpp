#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

using namespace std;

class Pessoa{
//    protected:
    private:
        string nome;
        long int cpf;
        string telefone;
        string email;
    public:
        Pessoa();
        ~Pessoa();
        string get_nome();
        void set_nome(string nome);
        long int get_cpf();
        void set_cpf(long int cpf);
        string get_telefone();
        void set_telefone(string telefone);
        string get_email();
        void set_email(string email);
        virtual void imprime_dados();
};

#endif











