#include "funcionario.hpp"
#include <iostream>

Funcionario::Funcionario(){
    cout << "Construtor da classe Funcionário" << endl;
    set_salario(0);
    set_funcao("");
}
Funcionario::Funcionario(string nome, long int cpf, string telefone, string email, float salario, string funcao){
    cout << "Construtor da classe Funcionário - Sobrecarga" << endl;
    set_nome(nome);
    set_cpf(cpf);
    set_telefone(telefone);
    set_email(email);
    set_salario(salario);
    set_funcao(funcao);
}
Funcionario::~Funcionario(){
    cout << "Destrutor da classe Funcionário" << endl;
}
float Funcionario::get_salario(){
    return salario;
}
void Funcionario::set_salario(float salario){
    this->salario = salario;
}
string Funcionario::get_funcao(){
    return funcao;
}
void Funcionario::set_funcao(string funcao){
    this->funcao = funcao;
}
void Funcionario::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Telefone: " << get_telefone() << endl;    
    cout << "Email: " << get_email() << endl;
    cout << "Salário: " << get_salario() << endl;
    cout << "Função: " << get_funcao() << endl;
} 








